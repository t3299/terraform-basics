#Deploy vpc in default aws region
resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" = "myvpc-us-east-1"
  }
}

#Deploy vpc in us-west-1 region
resource "aws_vpc" "us-west-1-region" {
  cidr_block = "10.1.0.0/16"
  provider=aws.aws-us-west-2
  tags = {
    "Name" = "myvpc-us-west-2"
  }
}