
#Terraform configuration block
terraform {
  required_version="~> 1.0.4"
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
      }
  }
}

#Providers block (us-east-1)
provider "aws" {
    region = "us-east-1"
    profile = "debrah"
}

#Providers block (us-west-1)
provider "aws" {
  region = "us-west-2"
  profile = "debrah"
  alias = "aws-us-west-2"
}