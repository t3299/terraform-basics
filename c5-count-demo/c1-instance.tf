# Resource-8: Create EC2 Instance
resource "aws_instance" "my-ec2-vm" {
  ami                    = "ami-087c17d1fe0178315" # Amazon Linux
  instance_type          = "t2.micro"
  count = 5
  tags = {
    "Name" = count.index
  }    
}



