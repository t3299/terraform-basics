#Define terraform settings
terraform {
  required_version = "~> 1.0.4"
  required_providers {
    aws = {
      version = ">= 2.0"
      source  = "hashicorp/aws"
    }
  }
}

#Define default provider
provider "aws" {
  region  = "us-east-1"
  profile = "debrah"
}