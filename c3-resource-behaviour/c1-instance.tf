#Define resource
resource "aws_instance" "my-ec2-vm" {
  ami           = "ami-087c17d1fe0178315"
  instance_type = "t2.micro"
  #   availability_zone = "us-east-1a"
  availability_zone = "us-east-1b"
  tags = {
    "Name" : "web"
    "tag1" : "my-second-tag"
  }
}