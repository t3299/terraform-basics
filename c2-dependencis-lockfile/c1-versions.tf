#Provider settings
terraform {
    required_version = "~> 1.0.4"
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = ">= 2.0"
        }
        random = {
            source = "hashicorp/aws"
            version = ">= 3.0"
        }
    }
}

#Default provider
provider "aws" {
  region = "us-east-1" #Default
  profile = "debrah"
}

#Random provider
provider "random" {
  region = "us-west-2"
  profile = "debrah"
}