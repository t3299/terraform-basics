#Generate random values
resource "random_string" "random" {
  length = 5
  seperator = "-"
}

#s3 bucket definition
resource "aws_s3_bucket" "mys3" {
  bucket = "fastbidgh-s3-${random_string.random.id}"
}